import React, { Component, Fragment } from 'react';
// import PropTypes from 'prop-types';
import WizardFormFirstPage from './Pages/WizardFormFirstPage';
import WizardFormSecondPage from './Pages/WizardFormSecondPage';
import PageResultat from './Pages/PageResultat';
import SubtitleForm from './Components/SubtitleForm';
import PageLayout from './Layouts/PageLayout';

import StepsForm from "./Components/StepsForm";

import { translate } from "react-i18next";

class WizardForm extends Component {

  state = {
    page: 1
  };

  nextPage = () => {
    this.setState({ page: this.state.page + 1 });
  };

  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  };

  render() {
    const { onSubmit, t} = this.props;
    const { page } = this.state;

    return <Fragment>
        <h1 className="title is-2 has-text-centered">
          {t('TitleForm')}
        </h1>
        <SubtitleForm text={t("SubtitleForm")} />

        <StepsForm page={page} />

        {page === 1 && <WizardFormFirstPage onSubmit={this.nextPage} />}
        {page === 2 && <WizardFormSecondPage previousPage={this.previousPage} onSubmit={this.nextPage} />}
        {page === 3 && <PageResultat onSubmit={onSubmit} />}
      </Fragment>;
  }
}

WizardForm = translate()(WizardForm);

export default PageLayout(WizardForm);
