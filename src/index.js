import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import dotenv from 'dotenv';
import store from './Tools/store';
import showResults from './Tools/showResults';
import WizardForm from './WizardForm';
import './WizardForm.css';

// Internationalisation aka i18n
import { I18nextProvider } from 'react-i18next';
import i18n from './i18n';

const root = document.getElementById('root');

dotenv.config();

ReactDOM.render(
  <Provider store={store}>
    <I18nextProvider i18n={i18n}>
      <WizardForm onSubmit={showResults} />
    </I18nextProvider>
  </Provider>,
  root
);
