import i18next from 'i18next';
import LanguageDetector from "i18next-browser-languagedetector";

import fr from "../i18n/fr.json"
import en from "../i18n/en.json"

i18next.use(LanguageDetector).init({
  // Using simple hardcoded resources for simple example
  resources: {
    fr,
    en
  },
  detection: {
    order: ["navigator"]
  },
  fallbackLng: "fr",
  // have a common namespace used around the full app
  keySeparator: false, // we use content as keys
  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ","
  },
  react: {
    wait: true
  }
});

export default i18next