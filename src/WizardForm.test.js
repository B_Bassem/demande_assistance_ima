import React from 'react';
import { render } from 'react-testing-library'
import WizardForm from "./WizardForm";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import { Provider } from "react-redux";
import store from "./Tools/store";

describe("WizardForm", () => {
	it("renders the WizardForm", () => {
		const { getByText, queryByText } = render(
			<Provider store={store}>
				<I18nextProvider i18n={i18n}>
					<WizardForm />
				</I18nextProvider>
			</Provider>
	);
		const title = queryByText("Make a request for assistance");
		expect(title.innerHTML).toBe("Make a request for assistance");
		// const title = getByText('Effectuer une demande d\'assistance')
		// expect(title.innerHTML).toBe('Effectuer une demande d\'assistance')
	});
});