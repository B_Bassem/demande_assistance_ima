import React from 'react';

const SubtitleForm = ({ text }) => {
  return <h2 className="subtitle has-text-centered">{text}</h2>;
};

export default SubtitleForm;
