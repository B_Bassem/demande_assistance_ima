import React from "react";

const StepsForm = ({ page }) => (
  <ul className="steps is-narrow is-medium is-centered has-content-centered is-horizontal is-hidden-mobile">
    <li className={page === 1 ? "steps-segment is-active" : "steps-segment"}>
      <a className="has-text-dark">
        <span className="steps-marker">
          <span className="icon">
            <i className="far fa-user" />
          </span>
        </span>
        <div className="steps-content">
          <p className="heading">Information Utilisateur</p>
        </div>
      </a>
    </li>

    <li
      className={
        page === 2
          ? "steps-segment is-active has-gaps"
          : "steps-segment has-gaps"
      }
    >
      <span className="steps-marker">
        <span className="icon">
          <i className="fas fa-hands-helping" />
        </span>
      </span>
      <div className="steps-content">
        <p className="heading">Information Assistance</p>
      </div>
    </li>

    <li className={page === 3 ? "steps-segment is-active" : "steps-segment"}>
      <span className="steps-marker is-hollow">
        <span className="icon">
          <i className="fas fa-check" />
        </span>
      </span>
      <div className="steps-content">
        <p className="heading">Confirmation</p>
      </div>
    </li>
  </ul>
);

export default StepsForm;
