import React from 'react';

const MySelect = ({ label }) => {
    return (
        <div className="field">
            <label className="label">{label}</label>
            <div className="control">
                <div className="select is-fullwidth">
                    <select>
                        <option>&nbsp;Monsieur</option>
                        <option>&nbsp;Madame</option>
                    </select>
                </div>
            </div>
        </div>
    );
};

export default MySelect;