import React from 'react';

const Myinput = ({input, label, type, disabled, meta: { touched, error }}) => {
    return (
        <div className="field">
            <label className="label">{label}</label>
            <div className="control">
                <input {...input} className="input" type={type} disabled={disabled}/>
            </div>
            {touched && error && <span className="subtitle has-text-danger">{error}</span>}
        </div>
            );
        };
        
export default Myinput;