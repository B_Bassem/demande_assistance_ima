import React from 'react';

const TextAreaInput = () => {
  return (
    <textarea
      className="textarea"
      placeholder="Expliquez nous votre problème et nous vous rappellerons"
      rows={10}
    />
  );
};

export default TextAreaInput;
