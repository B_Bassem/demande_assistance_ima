import React, { Fragment } from 'react';

const renderField = ({ input, type, meta: { touched, error } }) => (
  <Fragment>
    <input {...input} type={type} />
    {touched && error && <span className="subtitle has-text-danger">{error}</span>}
  </Fragment>
);

export default renderField;
