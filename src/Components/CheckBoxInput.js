import React from 'react';

const CheckBoxInput = ({input, label, checked, type}) => {
    return (
        <label className="checkbox">
            <input {...input} type={type} defaultChecked={checked}/>
            {label}
        </label>
    );
};

export default CheckBoxInput;