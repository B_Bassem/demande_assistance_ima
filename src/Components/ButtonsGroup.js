import React from 'react';

const ButtonsGroup = ({ button1, button2 }) => (
  <div className="field is-grouped is-grouped-right">
    <p className="control">
      <button
        className={button1.className}
        type={button1.type}
        disabled={button1.disabled}
        onClick={button1.onClick}
      >
        {button1.text}
      </button>
    </p>
    <p className="control">
      <button
        className={button2.className}
        type={button2.type}
        disabled={button2.disabled}
        onClick={button2.onClick}
      >
        {button2.text}
      </button>
    </p>
  </div>
);

export default ButtonsGroup;
