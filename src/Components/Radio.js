import React from 'react';

const Radio = ({ label,  meta: { touched, error } }) => {
    return (
        <div className="field">
            <label className="label">{label}</label>
            <div className="control">
                <label className="radio">
                    <input type="radio" name="sex" />
                    &nbsp;Monsieur
                </label>
                <label className="radio">
                    <input type="radio" name="sex" />
                    &nbsp;Madame
                </label>
            </div>
            {touched && error && <span className="subtitle has-text-danger">{error}</span>}
        </div>

    );
};

export default Radio;