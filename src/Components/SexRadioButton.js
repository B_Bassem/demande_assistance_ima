import React from 'react';
import { Field } from 'redux-form';
import uuidv4 from 'uuid/v4';
import RenderField from './renderField';

const SexRadioButton = ({ data }) => {
  const { label, type, options, radioButtonName, validate } = data;
  return (
    <div className="field">
      <label className="label">{label}</label>
      <div className="control">
        {options.map(item => (
          <label className={type} key={uuidv4()}>
            <Field
              name={radioButtonName}
              component={RenderField}
              type={type}
              value={item.value}
              validate={validate}
            />
            {item.text}
          </label>
        ))}
      </div>
    </div>
  );
};

export default SexRadioButton;
