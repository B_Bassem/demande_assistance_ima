import React from "react";
import { reduxForm } from "redux-form";
import FormLayout from "../Layouts/FormLayout";

let PageResultat = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className="columns">
        <div className="column">
          <p className="title is-5 has-text-info has-text-centered">
            Votre demande d'assistance a été prise en compte.<br /> Un
            conseiller vas vous rappeler sous 48h pour vous proposez des
            solutions adaptées à votre situation.
          </p>
        </div>
      </div>

      <div className="columns">
        <div className="column">
          <div className="field is-grouped is-grouped-right">
            <p className="control">
              <button className="button is-info" type="submit">
                Fermer
              </button>
            </p>
          </div>
        </div>
      </div>
    </form>
  );
};

PageResultat = reduxForm({
  form: "wizard", //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(PageResultat);

export default FormLayout(PageResultat);
