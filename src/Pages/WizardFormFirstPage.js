import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import {
  required,
  maxLength30,
  maxLength50,
  maxLength200,
  tel,
  email,
  dateValid
} from "../Tools/validate";
import MyInput from '../Components/MyInput';
import SexRadioButton from '../Components/SexRadioButton';
import FormLayout from '../Layouts/FormLayout';
import ButtonsGroup from '../Components/ButtonsGroup';

// Fake data for testing
const data = {
  lastName: "Jane",
  firstName: "Doe",
  tel: "0623568974",
  date_naissance: "2010-07-21",
  email: "jdoe@gmail.com",
  genre: "F",
  // inputRefDossier:
  //   "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget neque velit. Nullam ut gravida augue, et dapibus metus. Duis non dui metus. Integer et purus ornare, dapibus ex a, porttitor sem. Maecenas turpis augue, mattis at vehicula nec, eleifend eget sapien. Sed quis lectus imperdiet, efficitur nunc sit amet, gravida mauris. Fusce auctor libero dolor, in dignissim lacus accumsan a. Aliquam tempor, sapien sit amet egestas euismod, sem lectus euismod metus, sit amet semper enim leo ac risus. Ut non turpis nec urna posuere lobortis."
};

let WizardFormFirstPage = props => {
  const { handleSubmit, pristine, reset, submitting } = props;

  const genreRadioButton = {
    label: 'Civilité',
    type: 'radio',
    radioButtonName: 'genre',
    component: 'input',
    options: [
      {
        text: '\xa0Monsieur',
        value: 'H'
      },
      {
        text: '\xa0Madame',
        value: 'F'
      }
    ],
    validate: [required]
  };

  const lastnameInput = {
    label: 'Nom',
    type: 'text',
    validate: [required, maxLength30]
  };

  const nameInput = {
    label: 'Prénom',
    type: 'text',
    validate: [required, maxLength50]
  };

  const dobInput = {
    label: 'Date de Naissance',
    type: 'date',
    validate: [required, dateValid]
  };

  const telInput = {
    label: 'Téléphone de Rappel',
    type: 'tel',
    validate: [required, tel]
  };

  const emailInput = {
    label: 'Adresse Email',
    type: 'text',
    validate: [required, email,maxLength200]
  };

  const buttonCancel = {
    className: 'button',
    disabled: pristine || submitting,
    onClick: reset,
    text: 'Annuler'
  };

  const buttonNext = {
    className: 'button is-primary',
    type: 'submit',
    disabled: submitting,
    text: 'Suivant'
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="columns">
        <div className="column">
          <SexRadioButton data={genreRadioButton} />
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <Field
            name="lastName"
            component={MyInput}
            props={lastnameInput}
            validate={lastnameInput.validate}
          />
        </div>
        <div className="column">
          <Field
            name="firstName"
            component={MyInput}
            props={nameInput}
            validate={nameInput.validate}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <Field
            name="date_naissance"
            component={MyInput}
            props={dobInput}
            validate={dobInput.validate}
          />
        </div>
        <div className="column">
          <Field
            name="tel"
            component={MyInput}
            props={telInput}
            validate={telInput.validate}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <Field
            name="email"
            component={MyInput}
            props={emailInput}
            validate={emailInput.validate}
          />
        </div>
      </div>
      <ButtonsGroup button1={buttonCancel} button2={buttonNext} />
    </form>
  );
};

WizardFormFirstPage = reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(WizardFormFirstPage);

WizardFormFirstPage = connect(state => ({
  initialValues: data
}))(WizardFormFirstPage);

export default FormLayout(WizardFormFirstPage);
