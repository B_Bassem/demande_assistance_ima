import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, formValueSelector, getFormSyncErrors } from "redux-form";
import CheckBoxInput from "../Components/CheckBoxInput";
import MyInput from "../Components/MyInput";
import TextAreaInput from "../Components/TextAreaInput";
import { required, maxLength11, maxLength1000 } from "../Tools/validate";
import FormLayout from "../Layouts/FormLayout";
import ButtonsGroup from "../Components/ButtonsGroup";

class WizardFormSecondPage extends Component {
  state = {
    checkbox: false
  }

  componentDidUpdate() {
    
  }

  changeValue = () => {

    console.log("Val", this.props.addSyncErrorsProp.syncErrors);
    
    let { dossierReferanceCB } = this.props;
    if (dossierReferanceCB === undefined) {
      dossierReferanceCB = true;
    } 
    
    this.setState({
      checkbox: dossierReferanceCB
    });
  }

  render() {
    const {
      dossierOuvertCB,
      dossierReferanceCB,
      handleSubmit,
      previousPage,
      submitting
    } = this.props;

    const checkboxDossier = {
      name: "checkboxDossier",
      label: "\xa0J'ai déja ouvert un dossier pour cette demande",
      type: "checkbox",
      checked: false
    };
    const inputRefDossier = {
      name: "inputRefDossier",
      label: "Référence du dossier",
      type: "text",
      disabled: dossierReferanceCB,
      validate: [required, maxLength11]
    };
    const checkboxNumeroDossier = {
      name: "checkboxNumeroDossier",
      label: "\xa0Je ne connais pas la référence du dossier",
      type: "checkbox",
      checked: false
    };
    const textArea = {
      name: "textArea",
      type: "textarea",
      validate: [maxLength1000]
    };
    const buttonPrevious = {
      type: "button",
      className: "button",
      onClick: previousPage,
      text: "Précédent"
    };
    const buttonSubmit = {
      type: "submit",
      className: "button is-primary",
      disabled: submitting, // pristine ?
      text: "Valider"
    };



    return <form onSubmit={handleSubmit}>
        <div className="columns">
          <div className="column">
            <Field
              name={checkboxDossier.name}
              component={CheckBoxInput}
              props={checkboxDossier}
            />
          </div>
        </div>

        {dossierOuvertCB && <Fragment>
              <div className="columns">
                <div className="column">
                  {this.state.checkbox ? (
                    <Field
                      name={inputRefDossier.name}
                      component={MyInput}
                      props={inputRefDossier}
                      validate={[]}
                    />
                  ) : (
                    <Field
                      name={inputRefDossier.name}
                      component={MyInput}
                      props={inputRefDossier}
                      validate={inputRefDossier.validate}
                    />
                  )}
                </div>
              </div>

              <div className="columns">
                <div className="column">
                  <Field name={checkboxNumeroDossier.name} component={CheckBoxInput} props={checkboxNumeroDossier} onChange={this.changeValue} />
                </div>
              </div>
            </Fragment>}

        <div className="columns">
          <div className="column">
            <Field name={textArea.name} component={TextAreaInput} props={textArea} validate={textArea.validate} />
          </div>
        </div>

        <ButtonsGroup button1={buttonPrevious} button2={buttonSubmit} />
      </form>;
  }
}

WizardFormSecondPage = reduxForm({
  form: "wizard", //Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(WizardFormSecondPage);

const selector = formValueSelector("wizard");
const addSyncErrorsProp = state => ({
  syncErrors: getFormSyncErrors('myForm')(state),
});


WizardFormSecondPage = connect(state => {
  const dossierOuvertCB = selector(state, "checkboxDossier");
  const dossierReferanceCB = selector(state, "checkboxNumeroDossier");
  return { 
    dossierOuvertCB, 
    dossierReferanceCB, 
    addSyncErrorsProp,
  };
})(WizardFormSecondPage);

export default FormLayout(WizardFormSecondPage);
