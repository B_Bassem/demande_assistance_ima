import moment from 'moment';

export const required = value => (value ? undefined : 'Requis');

const maxLength = max => value =>
  value && value.length > max
    ? `Il ne peut y avoir que ${max} charactères maximum`
    : undefined;
export const maxLength10 = maxLength(10);
export const maxLength11 = maxLength(11);
export const maxLength30 = maxLength(30);
export const maxLength50 = maxLength(50);
export const maxLength200 = maxLength(200);
export const maxLength1000 = maxLength(1000);

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Adresse email invalide'
    : undefined;

export const tel = value =>
  value && !/^(0|[0-7][0-9]{9})$/i.test(value)
    ? "Ceci n'est pas un numéro de téléphone valide"
    : undefined;

export const dateValid = value => {
  const parseDate = date => {
    return {
      year: date.year(),
      month: date.month(),
      day: date.day()
    };
  };
  const dob = parseDate(moment(value));
  const now = parseDate(moment());
  if (dob.year >= now.year && dob.month >= now.month && dob.day > now.day) {
    return 'La date doit être dans le passé';
  } else return undefined;
};
