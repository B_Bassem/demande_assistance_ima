const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default async (values) => {
  await sleep(300);
  window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
}