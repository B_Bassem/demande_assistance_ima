import React from 'react';

let PageLayout = Page => props => (
  <section className="section">
    <Page {...props} />
  </section>
);

export default PageLayout;
