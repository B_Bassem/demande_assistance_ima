import React from 'react';

let FormLayout = Form => props => (
  <div className="columns">
    <div className="column is-three-fifths is-offset-one-fifth is-half-desktop is-offset-one-quarter-desktop">
      {/* <div className="column is-three-fifths is-offset-one-fifth is-two-quarter-fullhd is-offset-two-quarter-fullhd"> */}
      <Form {...props} />
    </div>
  </div>
);

export default FormLayout;
