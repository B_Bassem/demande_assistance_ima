# Interface web de demande d'assistance digitale.

#### Dépot Gitlab : IMA / interactions / beneficiaires / demandeassistancesante-applicatif 

* Projet fait en `ReactJS` à base de create-react-app (voir README_CRA.md pour plus d'info).
* Le projet utilise `redux` (v3.0.0) pour la gestion de l'état de l'application et [redux-form](https://redux-form.com/7.3.0/) pour faciliter le développement d'un formulaire intéractif.
* Le CSS est gérer via [Bulma](bulma.io) et utilise les FlexBoxes.
* I18n est géré via `i18next` ([doc](https://www.i18next.com/)) et `react-i18next` (wrapper pour react de i18next) en utilisant l'API Context.

## Structure du projet
Tous les développement sont dans le dossier /src

* `Components` : Les composants unitaires du formulaire
* `i18n` : initialisation et fichier de configuration de lang pour le projet
* `Layouts` : Fichier de layouts qui englobe les pages pour externaliser les styles globaux. Développé sous forme de HOC (High Order Component).
* `Pages` : Les différentes pages du projets qui appelent les composants.
* `Tools` : Utilitaires comme le store `redux`, les fonctions de validation de surface du formulaire...
* `WizardForm.js` est le point d'entrée du projet.
* `WizardForm.scss` : fichier de style global. Ici nous pouvons surcharger les class CSS Bulma (voir exemple et la doc Bulma). Pour le style local privilègier le CSS-in-JS grâce à la `props` **style**.
* WizardFrom.css : Ce fichier est généré automatiquement. Ne pas le modifier car il est réécrit. Voir explication plus bas.
* (Exception) A la racine le fichier .env est lu au début du lancement du projet et set des varibles d'environnement si besoin.

## Script de lancement
> Privilègier l'utilisation de `yarn` plutôt que de `npm`.

Pour installer les modules : `yarn`

Pour lancer le projet en mode `dev` : `yarn start`

Pour faire un build de prod : `yarn build`. Exposer ensuite les fichiers ce trouvant dans `/build`.

## Note pour le SCSS
Create-react-app ne prend pas en charge par défaut les fichiers `.scss` sans utiliser la fonction `eject` (voir la doc/readme CRA) et donc en se passant du bénéfice d'utilisation du boilerplate. Les scripts `start` et `build` on donc été modifé pour charger les librairies SCSS et les varibles locales puis générer, avec hotreload pour le mode dev, un fichier `.css` correspondant. Les fichier .css sont inclus dans le `.gitignore` car générés à chaque utilisations.


